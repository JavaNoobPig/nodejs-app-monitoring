#建立Cluster:
eksctl create cluster

#取得目前現有哪些Cluster及其名稱:
eksctl get cluster

#對應課程結束移除(課上完才跑):
eksctl delete cluster --name {cluster名稱}

#這邊建置直接使用default的namespace建立要觀察的微服務系統:
kubectl apply -f config-best-practices.yaml

#安裝指令整理,增加Repo位置(如果本機沒做過的話):
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

#更新Repo內容(如果本機沒做過的話):
helm repo update

#建立Prometheus專用的namespace:
kubectl create namespace monitoring

#安裝Prometheus Chart,並給予名稱為monitoring,並安裝到monitoring ns裡面:
helm install {名稱,這邊一樣給monitoring} {Repo名稱}/{chart名稱,叫kube-prometheus-stack} -n {指定的namespace}

#整理上述對應的指令即如下(大概10分鐘),有時候久了會清cache,要重做上面update:
helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring

#將Prometheus Web UI服務可以透過本機連通,把本機對應PORT往前推到遠端服務:
kubectl port-forward {元件名稱} {執行kubectl指令本機PORT}:{這個元件PORT} -n monitoring &

#上面Prometheus Web UI使用PORT是9090,所以組出來的指令類似:
kubectl port-forward service/monitoring-kube-prometheus-prometheus 9090:9090 -n monitoring &

#上述執行後即可訪問本機127.0.0.1:9090  會自動導到遠端Prometheus Web#同理Grafana的Web UI:
kubectl port-forward service/monitoring-grafana 8080:80 -n monitoring &

#上述執行後,可以瀏覽127.0.0.1:8080,預設帳密是admin/prom-operator
#透過kubectl工具建立寫好的Alert Rule(檔案內已經有寫指定的namespace了):
kubectl apply -f alert-rules.yaml

#確認現有Alert Rule方式:
kubectl get PrometheusRule -n monitoring

#查看建立Alert Rule的過程,指定到Container:
kubectl logs prometheus-monitoring-kube-prometheus-prometheus-0 -n monitoring -c config-reloader

#另一個主要Container的log也有建立Alert Rule的log可以看:
kubectl logs prometheus-monitoring-kube-prometheus-prometheus-0 -n monitoring -c prometheus

#起一個cpu壓測的pod(吃4core,持續30秒):
kubectl run cpu-test --image=docker.io/containerstack/cpustress -- --cpu 4 --timeout 30s --metrics-brief
#############Alertmanager部分#####################

#查找Alertmanager的service:
kubectl get svc -n monitoring

#將Alertmanager的服務進行forward:
kubectl port-forward svc/monitoring-kube-prometheus-alertmanager 9093:9093 -n monitoring &

#透過以下指令拿到Alertmanager完整設定檔內容(但是有base64加密):
kubectl get secret alertmanager-monitoring-kube-prometheus-alertmanager-generated -n monitoring -o yaml

#後面就是自定義設定檔的部分:
#如果像講師畫面一樣只有alertmanager.yaml,沒有gz的話,可透過以下bash指令解密:
echo {後面那一長串} | base64 --decode

#如上,我的看到是"alertmanager.yaml.gz",所以解密要變成:
echo {後面那一長串} | base64 --decode | gzip -d
#建立寄送通知使用的gmail的密碼,但首先要先對密碼做base64加密,放到Secret的YAML:
echo "密碼" | base64

#寫好Secret後建立:
kubectl apply -f email-secret.yaml

#然後寫好Alertmanager的Config的YAML執行建立:
kubectl apply -f alert-manager-configuration.yaml

#確認上述建立的物件:
kubectl get alertmanagerconfig -n monitoring

#確認先找到Alertmanager的Pod:
kubectl get pod -n monitoring

#執行查看pod內容,順便看看有甚麼container在裡面:
kubectl describe pod alertmanager-monitoring-kube-prometheus-alertmanager-0 -n monitoring

#找到Pod後就可以查看log,先看alertmanager:
kubectl logs alertmanager-monitoring-kube-prometheus-alertmanager-0 -c alertmanager -n monitoring

#再看config-reloader:
kubectl logs alertmanager-monitoring-kube-prometheus-alertmanager-0 -c config-reloader -n monitoring

#建立個會觸發CPU偏高的Alert的pod(吃4core,持續60秒):
kubectl run cpu-test --image=docker.io/containerstack/cpustress -- --cpu 4 --timeout 60s --metrics-brief

#查看Alertmanager裡面的log,應該會寫:Username & password等問題:
kubectl logs alertmanager-monitoring-kube-prometheus-alertmanager-0 -c alertmanager -n monitoring

#這邊可以看看其他exporter範例對應的ServiceMonitor元件,先找到隨便一個元件:
kubectl get servicemonitor -n monitoring

#查看其中一個的設定:
kubectl get servicemonitor {隨便挑一個名稱} -n monitoring -o yaml

#查找redis佈署的service元件的名稱:
kubectl get svc |grep redis

#這邊整理Helm要做的指令,首先新增Chart:
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

#安裝這個Redis Exporter的Chart,其中[RELEASE_NAME]改為"redis-exporter",並且要帶我們設置好的values.yaml:
helm install redis-exporter prometheus-community/prometheus-redis-exporter -f redis-values.yaml

#接著可以透過helm工具確認:
helm ls

#然後也可以確認redis exporter的pod,會多出redis exporter:
kubectl get pod

#也可以直接地看servicemonitor元件:
kubectl get servicemonitor

#也可以順便看看他的設定yaml,也可以看到會有labels是release: monitor對應,底下還會有target位置:
kubectl get servicemonitor redis-exporter-prometheus-redis-exporter -o yaml

#其中上面對應的Endpoint是redis-exporter的svc對應的,可以用指令看:
kubectl describe svc redis-exporter-prometheus-redis-exporter

#執行Alert Rule建置:
kubectl apply -f redis-rules.yaml

#這邊可以透過指令確認(在default ns):
kubectl get prometheusrule

#直接修改Redis的deployment元件的replica數量到0:
kubectl edit deployment redis-cart

#觀察redis的pod:
kubectl get pod

#直接修改Redis的deployment元件的replica數量到1:
kubectl edit deployment redis-cart

#可以看看Redis Exporter Server的IP位置,先拿到Server名稱:
kubectl get svc

#然後看到Redis Exporter的Server元件內對應的Endpoint位置:
kubectl describe svc redis-exporter-prometheus-redis-exporter

#執行以下node指令即可在本機看到網頁:
cd {到有package.json目錄}
npm install
node ./app/server.js

#執行以下指令進行image封裝(記得改帳號):
docker build -t nanajanashia/demo-app:nodeapp .

#確認:
docker images

#登入(填帳號密碼):
docker login
#推(記得改帳號部分):
docker push nanajanashia/demo-app:nodeapp

#取得docker server位置,等等後面建立拉image的secret要用:
docker info
#如上這時候有一行是Registry的位置的URL
#簡單建立取用image的帳密SECRET:
kubectl create secret docker-registry my-registry-key --docker-server={上面Registry的URL值} --docker-username=nanajanashia --docker-password={這邊填密碼}
#上面指令實際組出來就是:
kubectl create secret docker-registry my-registry-key --docker-server=https://index.docker.io/v1/ --docker-username=nanajanashia --docker-password={密碼}

#接著就是佈署上述兩個K8S元件:
kubectl apply -f k8s-config.yaml

#這時候可以確認(這邊就沒有再佈署google的微服務專案了,比較乾淨):
kubectl get pod
#確認service:
kubectl get svc

#快速本機確認:
kubectl port-forward svc/nodeapp 3000:3000

#快速調整成自己dockerhub的image:
kubectl apply -f k8s-config.yaml

#執行以下指令建立元件(補上serviceMonitor元件):
kubectl apply -f k8s-config.yaml
